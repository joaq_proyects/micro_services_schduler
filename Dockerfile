
FROM python:3-onbuild
RUN pip install -r requirements.txt
EXPOSE 8081
ENV frecuencia=diaria
ADD . /src
COPY /src .
WORKDIR .
CMD ["gunicorn", "-b", "0.0.0.0:8081", "service_scheduler"]

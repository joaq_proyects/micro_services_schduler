
import json, time, threading, datetime, os
import requests, schedule, falcon
import settings

class Recuperador_informacion(object):
    cron_ejec = dict()
    servicio_disponible = True
    estados_informacion = dict()
    info_recuperada = None # OK;PENDIENTE;ERR;NO_PROGRAMADA
    url_servicio = None

    def __init__(self, cron_ejec):
        self.cron_ejec = cron_ejec
        self.servicio_disponible = True
        if(self.cron_ejec["tipo"] == "mensual"):
            self.info_recuperada = "NO_PROGRAMADA"
        else:
            self.info_recuperada = "PENDIENTE"

    def on_post(self, req, res):
        try:
            data = json.loads(req.stream.read().decode())
            disponible = data["disponible"]
            # if( type(data["disponible"]) is types.BooleanType ):
            if( disponible == False or disponible.upper() == "FALSE"):
                self.servicio_disponible = False
            elif( disponible == True or disponible.upper() == "TRUE"):
                self.servicio_disponible = True
            res.status = falcon.HTTP_200
            res.body = "Se modifico correctamente"
        except Exception as ex:
            print("Error en on_post:" + str(ex))
            res.status = falcon.HTTP_500
            res.body = "Error al modificar:" + str(ex)

    def on_get(self, req, res):
        try:
            res.status = falcon.HTTP_200
            res.body = "Conexion al servicio hoy:" + self.info_recuperada
        except Exception as ex:
            res.status = falcon.HTTP_500
            res.body = "Error al consultar conexion al servicio"

    def fin_dia(self):
        self.info_recuperada = "PENDIENTE"

    def fin_mes(self):
        hoy = datetime.date.today()
        # VUELVO A PENDIENTE AL CUMPLIRSE EL MES DESDE LA ULTIMA EJECUCION
        if(self.cron_ejec["dia"] == str(hoy.day)):
            self.info_recuperada = "PENDIENTE"

    def recuperar_informacion(self):
        resp = {"resultado": self.info_recuperada, "estado_servicio": str(self.servicio_disponible), "info": ""}
        if(resp["resultado"] == "PENDIENTE" and self.servicio_disponible):
            req = requests.get(self.cron_ejec["url_servicio"])
            resp = dict()
            if(req.status_code == 200):
                self.info_recuperada = "OK"
                self.ultima_ejecucion = datetime.date.today()
                try:
                    pass
                    # PENDIENTE: ACA ME CONECTO CON LA BASE PARA GUARDAR LA INFORMACION RECUPERADA INFORMACION
                except:
                    self.info_recuperada = "ERR"
                resp = {"resultado": self.info_recuperada, "estado_servicio": str(self.servicio_disponible), "info": str(req.text)}
            else:
                resp = {"resultado": self.info_recuperada, "estado_servicio": str(self.servicio_disponible), "info": ""}
        print(resp)

    def programar_ejecuciones(self):
        metodos = {"diaria": self.fin_dia, "mensual": self.fin_mes}
        schedule.every().day.at(self.cron_ejec["hora"]).do(self.recuperar_informacion)
        # VUELVO ESTADO A PENDIENTE. EJECUTO POR PRIMERA VEZ, Y LUEGO DEJO PROGRAMADO
        metodos[self.cron_ejec["tipo"]]()
        schedule.every().day.at("00:00:00").do(metodos[self.cron_ejec["tipo"]])
        while(True):
            schedule.run_pending()
            time.sleep(2)

frecuencia = os.environ["frecuencia"] # diaria / mensual
cron_ejec = settings.cron_ejec[frecuencia]
api = application = falcon.API()
try:
    print("Comienzo ejecucion")
    obj = Recuperador_informacion(cron_ejec)
    (threading.Thread(target = obj.programar_ejecuciones)).start()

    api.add_route("/servicios", obj)

except Exception as ex:
    print("Hubo un error durante la ejecucion:" + str(ex))
    exit()
